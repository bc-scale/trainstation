.. raw:: html

  <p>
  <a href="https://badge.fury.io/py/trainstation"><img src="https://badge.fury.io/py/trainstation.svg" alt="PyPI version" height="18"></a>
  </p>


:program:`trainstation` — Convenient training of linear models
**************************************************************

:program:`trainstation` is a tool that allows the efficient construction of linear models [FraEriErh20]_.
It provides supplementary functionality for handling data and model metrics and relies on the :program:`scikit-learn` library for the actual training [PedVarGra11]_.
:program:`trainstation` has been originally developed to aid the construction of force constant (via `hiphive <https://hiphive.materialsmodeling.org/>`_ [EriFraErh19]_) and cluster expansions (via `icet <https://icet.materialsmodeling.org/>`_ [AngMunRah19]_).
Since it has proven to be valuable in more general contexts and to simplify the maintenance of the two aforementioned packages, the functionality has been moved into a separate package.
Many examples for the application of :program:`trainstation` can be found in the documentation of `hiphive <https://hiphive.materialsmodeling.org/>`_ and `icet <https://icet.materialsmodeling.org/>`_.

:program:`trainstation` has been developed  at the `Department of Physics <https://www.chalmers.se/en/departments/physics/Pages/default.aspx>`_
of `Chalmers University of Technology <https://www.chalmers.se/>`_ in Gothenburg, Sweden.
Please consult the :ref:`credits page <credits>` for information on how to cite :program:`trainstation`.

:program:`trainstation` and its development are hosted on `gitlab <https://gitlab.com/materials-modeling/trainstation>`_.
Bugs should be submitted via the `gitlab issue tracker <https://gitlab.com/materials-modeling/trainstation/issues>`_.

.. toctree::
   :maxdepth: 2
   :caption: Main

   installation
   credits
   moduleref

.. toctree::
   :maxdepth: 2
   :caption: Backmatter

   bibliography
   glossary
   genindex
