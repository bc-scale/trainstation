.. index:: Glossary

Glossary
********

.. glossary::

   ARDR
        Automatic relevance determination regression (ARDR) is an optimization
        algorithm provided by `scikit-learn
        <https://scikit-learn.org/stable/modules/linear_model.html#automatic-relevance-determination-ard>`_
	[PedVarGra11]_

   compressive sensing
        `Compressive sensing (CS)
        <https://en.wikipedia.org/wiki/Compressed_sensing>`_, also known as
        compressive sampling, is an efficient method for constructing sparse
        solutions for linear systems.

   CV
   cross validation
        `Cross validation (CV)
        <https://en.wikipedia.org/wiki/Cross-validation_(statistics)>`_
        is commonly employed to evaluated the transferability and accuracy of
        linear problems.

   LASSO
        The `least absolute shrinkage and selection operator (LASSO)
        <https://en.wikipedia.org/wiki/Lasso_(statistics)>`_ is a method for
        performing variable selection and regularization in problems in
        statistics and machine learning.

   regularization
        `Regularization
        <https://en.wikipedia.org/wiki/Regularization_(mathematics)>`_,
        is commonly used in machine learning to combat overfitting and
        for solving underdetermined systems.

   RFE
        In machine learning `recursive feature elimination (RFE)
        <https://scikit-learn.org/stable/modules/generated/sklearn.feature_selection.RFE.html>`_
        is a popular `feature selection
        <https://en.wikipedia.org/wiki/Feature_selection>`_ process in model
        construction.

   RMSE
        `Root mean square error
        <https://en.wikipedia.org/wiki/Root-mean-square_deviation>`_,
        is a frequently measure for the deviation between a reference
        data set and a predicted data set.
