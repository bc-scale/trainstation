.. _installation:
.. index:: Installation

Installation
************

:program:`trainstation` can be installed via `pip`::

    pip3 install trainstation

If you want to get the absolutely latest version you can install from the repo::

    pip3 install -e git+https://gitlab.com/materials-modeling/trainstation#egg=trainstation

:program:`trainstation` requires Python3 and depends on the following libraries

* `scipy <https://www.scipy.org//>`_ (optimization)
* `scikit-learn <http://scikit-learn.org/>`_ (optimization)
* `NumPy <http://www.numpy.org/>`_ (linear algebra)
