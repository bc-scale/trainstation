image: registry.gitlab.com/materials-modeling/trainstation/cicd


variables:
  INSTDIR: "local_installation"


before_script:
  - export PYTHONPATH=$INSTDIR:$PYTHONPATH


build:linux:
  stage: build
  tags:
    - linux
  artifacts:
    paths:
      - $INSTDIR
  script:
    - pip3 install --target=$INSTDIR .


tests:linux:
  stage: test
  needs:
    - build:linux
  tags:
    - linux
  artifacts:
    paths:
      - htmlcov/
    reports:
      junit: report.xml
  script:
    - xdoctest trainstation
    - coverage run -m pytest --verbose --junitxml=report.xml tests/
    - coverage report -m
    - coverage html
  coverage: '/TOTAL.+ ([0-9]{1,3}%)/'


style_check:
  stage: test
  tags:
    - linux
  script:
    - flake8 trainstation/ tests/ doc/


test_documentation:
  stage: test
  needs:
    - build:linux
  tags:
    - linux
  script:
    - mkdir public
    - sphinx-build -b doctest -W doc/ public/
    - sphinx-build -W doc/ public/
  except:
   - master
  artifacts:
    paths:
      - public


include:
  - template: Code-Quality.gitlab-ci.yml

code_quality:
  artifacts:
    paths: [gl-code-quality-report.json]


pages:
  stage: deploy
  tags:
    - linux
  artifacts:
    paths:
      - public
  only:
    - master
    - tags
  script:
    - mkdir -p public/dev
    # code coverage report (for development version)
    - mv htmlcov/ public/
    - cp doc/conf.py doc/conf_original.py
    # --------------------------
    # DEVELOPMENT VERSION
    - tag=$(git describe | tail -1)
    - echo "tag= $tag"
    - sed -i "s/version = ''/version = '$tag'/" doc/conf.py
    - grep '^version' doc/conf.py
    - sphinx-build -W doc/ public/dev/
    # --------------------------
    # STABLE VERSION
    - tag=$(git tag | tail -1)
    - echo "tag= $tag"
    - git checkout $tag
    - cp doc/conf_original.py doc/conf.py
    - grep '^version' doc/conf.py
    - sphinx-build -W doc/ public/
    # --------------------------
    # clean up
    - ls -l public/
    - chmod go-rwX -R public/


pypi:
  stage: deploy
  tags:
    - linux
  only:
    - tags
  when: manual
  environment:
      name: pypi-upload
  script:
    # check out the latest tag (redundant if job is limited to tags; still a sensible precaution)
    - tag=$(git tag | tail -1)
    - echo "tag= $tag"
    - git checkout $tag
    # create source distribution and push to PyPI
    - python3 setup.py sdist
    - python3 setup.py bdist_wheel --universal
    - ls -l dist/
    - python3 -m twine upload dist/*
